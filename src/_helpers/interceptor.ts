import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from "@angular/common/http";
import {Observable} from "rxjs/internal/Observable";
import {Injectable} from "@angular/core";
import { tap } from 'rxjs/operators';
import {LoaderService} from '../services/loader.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private loaderService:LoaderService){}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      this.showLoader();
        let token = window.localStorage.getItem('token');
        if (token) {
          request = request.clone({
            setHeaders: {
              utoken:  token
            }
          });
        }
        // return next.handle(request);

        return next.handle(request).pipe(tap((event: HttpEvent<any>) => { 
          if (event instanceof HttpResponse) {
            this.hideLoader();
          }
        },
          (err: any) => {
            this.hideLoader();
        }));
      }


      private showLoader(): void {
        console.log("******showLoader******");
        this.loaderService.loadShow();
      }
      private hideLoader(): void {
        console.log("******hideLoader******");
        this.loaderService.loadHide(); 
      }
    
}

import { Injectable } from '@angular/core';
import Swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {

  constructor() { }
  loginSuccess() {
    const toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      animation: true,
      width: '100%',
      timer: 5000
    });
    toast.fire({
      icon: 'success',
      title: 'Signed in Successfully'
    })
  }
  logoutSuccess() {
    const toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      animation: true,
      width: '100%',
      timer: 5000
    });
    toast.fire({
      icon: 'success',
      title: 'Logged Out Successfully'
    })
  }
  toastMesaage(msgType,message){
    const toast = Swal.mixin({
      toast: true,
      position: 'top-end',
      showConfirmButton: false,
      animation: true,
      width: '100%',
      timer: 5000
    });
    toast.fire({
      icon: msgType,
      title: message
    })
  }
}

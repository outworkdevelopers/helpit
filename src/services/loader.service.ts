import { Injectable } from '@angular/core';
import { LoaderState } from '../model/loader-state';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoaderService {

  private loaderSubject = new Subject<LoaderState>();
  private loaderSubject1 = new Subject<LoaderState>();
  loaderState = this.loaderSubject.asObservable();
  loaderState1 = this.loaderSubject1.asObservable();
  constructor() { }

  show() {
    this.loaderSubject.next(<LoaderState>{show: true});
  }
  hide() {
      this.loaderSubject.next(<LoaderState>{show: false});
  }
  loadShow(){
      this.loaderSubject1.next(<LoaderState>{lodingShow: true});
  }
  loadHide(){
    this.loaderSubject1.next(<LoaderState>{lodingShow: false});
  }
}

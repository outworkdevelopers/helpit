import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from '../model/apiresponse.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RequestorService {

  constructor(private http: HttpClient) { }  
  baseUrl = environment.baseUrl;
  
  getRequestors(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'webapp/v1/admin/requestor/all');
  }
  addRequestor(RequestorPayload: any): Observable<ApiResponse> {
    console.log('add requestor',RequestorPayload);
    return this.http.post<ApiResponse>(this.baseUrl + 'webapp/v1/admin/requestor', RequestorPayload);
  }
}

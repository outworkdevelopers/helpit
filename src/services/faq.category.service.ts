import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from 'src/model/apiresponse.model';
import { environment } from 'src/environments/environment';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class FaqCategoryService {

  constructor(private http: HttpClient,private router: Router) {
     
  }  
 baseUrl = environment.baseUrl;

CreateFaqCategory(faqCategoryReqData: any): Observable<ApiResponse> {
  return this.http.post<ApiResponse>(this.baseUrl + 'webapp/v1/admin/faq/category/create',faqCategoryReqData);
}

getAllFaqCategories(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'webapp/v1/admin/faq/category/all');
  }

updateFaqCategoryById(updateData:any,faqcategoryintid:any):Observable<ApiResponse>{
  return this.http.put<ApiResponse>(this.baseUrl+'webapp/v1/admin/faq/category/'+faqcategoryintid+'/update',updateData)
}

Createtopic(topicReqData: any): Observable<ApiResponse> {
    return this.http.post<ApiResponse>(this.baseUrl + 'webapp/v1/admin/faq/category/topic',topicReqData);
  }

}

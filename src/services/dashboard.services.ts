import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs/index';
import { ApiResponse } from 'src/model/apiresponse.model';


@Injectable()
export class dashboardAPIService{
    constructor(private http:HttpClient){}

    baseUrl = environment.baseUrl;

    getTaskWeeklyCountInfo(startdate:string,enddate:string):Observable<ApiResponse>{
        return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/dashboard?startdate='+startdate+'&enddate='+enddate);  
    }

    getCurrentWeekTaskInfo(startdate:string,enddate:string):Observable<ApiResponse>{
        return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/dashboard/currentweek/taskinfo?startdate='+startdate+'&enddate='+enddate);  
    }

    getDashboardMasterInfo(selectedUserId:number):Observable<ApiResponse>{
        return this.http.get<ApiResponse>(this.baseUrl+'admin/v1/dashboard/info?id='+selectedUserId);
    }
}
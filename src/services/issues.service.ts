import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from 'src/model/apiresponse.model';
import { environment } from 'src/environments/environment';

import {Router} from "@angular/router";


@Injectable({
  providedIn: 'root'
})

export class IssuesService {
  constructor(private http: HttpClient,private router: Router) {
     
   }  
  baseUrl = environment.baseUrl;
  

  getIssues(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'webapp/v1/admin/issue/all');
  }


  addissue(IssuePayload: any): Observable<ApiResponse> {
    console.log('add issue',IssuePayload);
    return this.http.post<ApiResponse>(this.baseUrl + 'webapp/v1/admin/issue', IssuePayload);
  }
  getIssueById(Id: number): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'webapp/v1/admin/issue/issueid/' + Id);
  }

  // updateIssue(issue: any): Observable<ApiResponse> {
  //   let headers= new HttpHeaders().set('Access-Control-Allow-Origin', '*');
  //   return this.http.put<ApiResponse>(this.baseUrl + 'webapp/v1/admin/issue/update/issueid/'+ issue.Id + '/status/'+ issue.Status,
  //    issue);
  // }
  updateIssue(issue: any): Observable<ApiResponse> {
    let headers= new HttpHeaders().set('Access-Control-Allow-Origin', '*');

    console.log('issuedata: ', issue);
    return this.http.put<ApiResponse>(this.baseUrl + 'webapp/v1/admin/issue/update',issue);
  }

  
  getCategories(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'webapp/v1/admin/masterlist/categories');
  }

  getChannels(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'webapp/v1/admin/masterlist/channels');
  }

  getStatus(): Observable<ApiResponse> {
    return this.http.get<ApiResponse>(this.baseUrl + 'webapp/v1/admin/masterlist/status');
  }



}


import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/index';
import { ApiResponse } from 'src/model/apiresponse.model';
import { environment } from 'src/environments/environment';
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class FaqService {

  constructor(private http: HttpClient,private router: Router) {
     
  }  
 baseUrl = environment.baseUrl;

 getAllFaqs(categoryIntId:any,topicIntId:any): Observable<ApiResponse> {
  return this.http.get<ApiResponse>(this.baseUrl + 'webapp/v1/admin/faq/all?categoryIntId='+categoryIntId+ 
  '&topicIntId='+topicIntId);
 }

submitFaq(reqData: any): Observable<ApiResponse> {
  return this.http.post<ApiResponse>(this.baseUrl + 'webapp/v1/admin/faq',reqData);
}

getFaqById(faqintid): Observable<ApiResponse> {
  return this.http.get<ApiResponse>(this.baseUrl + 'webapp/v1/admin/faq/'+faqintid);
}

updateFaqById(updateData:any,faqIntId:any):Observable<ApiResponse>{
  return this.http.put<ApiResponse>(this.baseUrl+'webapp/v1/admin/faq/'+faqIntId+'/update',updateData)
}
}

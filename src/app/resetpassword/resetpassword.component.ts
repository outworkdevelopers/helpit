import { Component, OnInit, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { UserService } from '../../services/userservice.services';
import { AuthService } from '../guards/authguard/auth.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css']
})
export class ResetpasswordComponent implements OnInit {
  private _token: string;
  ResetForm: FormGroup;
  submitted=false;
  public password:any;
  loading:false;
  confirmationModal: BsModalRef;

  get token(): string {
    return this._token;
  }
  set token(value: string) {
    this._token = value;
  }

  error_messages = {    
    'password': [
      { type: 'required', message: 'password is required.' },
      { type: 'minlength', message: 'password must be at least 8 characters.' },
      { type: 'maxlength', message: 'password length.' }
    ],
    'confirmpassword': [
      { type: 'required', message: 'confirm password is required.' },
      { type: 'minlength', message: 'confirm password must be at least 8 characters.' },
      { type: 'maxlength', message: 'password length.' }
    ],
  }
  constructor(private formBuilder: FormBuilder, private router: Router, private activatedRoute: ActivatedRoute,
    private userService: UserService, private auth: AuthService,private modalService: BsModalService) {

      this.ResetForm = this.formBuilder.group({
        password: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(30)
        ])),
        confirmpassword: new FormControl('', Validators.compose([
          Validators.required,
          Validators.minLength(8),
          Validators.maxLength(30)
        ])),
      }, { 
        validators: this.password1.bind(this)
      });
     
  
     }
  get f() { return this.ResetForm.controls; }
  onResetSubmit(template: TemplateRef<any>) {
    
    this.submitted = true;
    if (this.ResetForm.invalid) {
      return;
    }
    let resetpassword = this.ResetForm.value.confirmpassword;
    this.userService.resetpassword(resetpassword, this.token)
      .subscribe(data => {
        if (data.Status === 'Success') {
          console.log("Reset Successful");
          this.confirmationModal = this.modalService.show(template, { class: 'modal-m mv-center',ignoreBackdropClick: true });   
         
        }
      });
  }

  closeConfirmationModal(){
    this.confirmationModal.hide();
    this.router.navigate(['/login']);
  }
  ngOnInit() {
    if (this.activatedRoute.snapshot.queryParamMap.has('token')) {
      this.token = this.activatedRoute.snapshot.queryParamMap.get('token');
    }
  }

  password1(formGroup: FormGroup) {
    const { value: password } = formGroup.get('password');
    const { value: confirmPassword } = formGroup.get('confirmpassword');
    return password === confirmPassword ? null : { passwordNotMatch: true };
  }

}

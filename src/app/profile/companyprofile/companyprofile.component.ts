import { Component, OnInit, OnDestroy } from '@angular/core';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import { ProfileApiService } from 'src/services/profileservice.services';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { EmailProcessApiService } from 'src/services/emailintegration.services';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-companyprofile',
  templateUrl: './companyprofile.component.html',
  styleUrls: ['./companyprofile.component.css']
})
export class CompanyprofileComponent implements OnInit,OnDestroy {
  companyname : string;
  emails:any;
  companyInfo:any;
  userEmailFlag:any;
  submitted = false;
  responseObject :any;
  disableButton:any;
  updateCompanyNameForm: FormGroup;
  loggedInUserRole:any;
  authCode:any;
  subscriptions:Subscription [] = [];
  get f() { return this.updateCompanyNameForm.controls; }
   constructor(private modalService: NgbModal,private apiService: ProfileApiService,
    private emailapiService:EmailProcessApiService,
    private formBuilder: FormBuilder,public auth: AuthService,private route: ActivatedRoute) { 

      this.loggedInUserRole = this.auth.getRole();
     }

   
  
  ngOnInit() {
   
    this.updateCompanyNameForm = this.formBuilder.group({     
      companyTitle: ['',Validators.compose([Validators.required])]      
    });

   let userProfileSub = this.apiService.getCompanyProfileInfoById()
      .subscribe(data => {
        if (data.Status === 'Success') {
          this.companyInfo = JSON.parse(data.Data)
        }
      });
this.subscriptions.push(userProfileSub);    

      let userDetails = localStorage.getItem('userInfo');
      let userDetails1 = JSON.parse(userDetails);
      this.userEmailFlag =userDetails1.IsEmailConfigured;
      if(this.userEmailFlag){
        this.disableButton=true;
      }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub)=>sub.unsubscribe());
  }

  openModal(updateCompanyInfoPopUp,name) {
    this.modalService.open(updateCompanyInfoPopUp, { centered: true, scrollable: true, backdrop: "static", keyboard: false });
    this.companyname =name;
  }

  
  updatecompanyname() {
    this.submitted = true;
    if (this.updateCompanyNameForm.invalid) {
      return;
    }
    this.updateCompanyNameForm.value.companyId = this.companyInfo.CompanyId;
    this.companyInfo.CompanyTitle = this.updateCompanyNameForm.value.companyTitle;
    this.apiService.updateCompanyProfileInfo(this.updateCompanyNameForm.value).subscribe(data => {
      if (data.Status === 'Success') {
        this.modalService.dismissAll();
      }
    });
  }     
  ConfigureEmail(){
    this.emailapiService.setUpEmailForAuthorization(this.emails)
    .subscribe(data => {
      if (data.Status === 'Success') {
        alert(data.Data);
        this.disableButton=true;
      }
      else{
        console.log(data.Message);
      }
    });
  }

  signIn() {
    this.authCode=this.route.snapshot.queryParams.code;
    console.log(this.authCode);
  if(this.authCode!=null||this.authCode!=''){
    this.emailapiService.CreateEmailOauthDetails(this.authCode)
      .subscribe(data => {
       if(data.Status === 'Success'&&data.Data!=""){
         console.log(data.Data);
       }
       else
       alert('Something went wrong while creating');
      });
    }
  else{
    alert('Something went wrong');
  }
  }

}

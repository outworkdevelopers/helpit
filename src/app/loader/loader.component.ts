import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { LoaderService } from '../../services/loader.service';
import { LoaderState } from '../../model/loader-state';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit {

  show = false;
  loadingShow = false;
  private subscription: Subscription;
  private loadsubscription: Subscription;
  constructor(private loaderService: LoaderService) { }

  ngOnInit() {
    this.subscription = this.loaderService.loaderState
            .subscribe((state: LoaderState) => {
                this.show = state.show;
            });
    this.loadsubscription = this.loaderService.loaderState1
            .subscribe((state: LoaderState) => {
                this.loadingShow = state.lodingShow;
            });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.loadsubscription.unsubscribe();
}

}

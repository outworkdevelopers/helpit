import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/userservice.services';
import { AuthService } from '../guards/authguard/auth.service';
@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css']
})
export class ForgotpasswordComponent implements OnInit {
  forgotForm: FormGroup;
  submitted = false;
  success = false;
  validation = false;
  get f() { return this.forgotForm.controls; }
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService, private auth: AuthService) { }
  forgotPassword() {
    this.submitted = true;
    if (this.forgotForm.invalid) {
      return;
    }
    this.validation = false;
    this.userService.forgotpassword(this.forgotForm.controls.loginId.value).subscribe(data => {
      if (data.Code == 'SUC-200') {
        this.success = true;
      
        // this.router.navigate(['/login']);
      }
      if (data.Code == 'SUC-301') {
        this.validation = true;
      }
    });
  }
  ngOnInit() {
    this.forgotForm = this.formBuilder.group({
      loginId: ['', Validators.compose([Validators.required])]
    });
  }

}

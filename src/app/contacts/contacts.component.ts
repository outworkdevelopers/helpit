import { Component, OnInit, ElementRef, ViewChild, OnDestroy, TemplateRef } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { RequestorService } from "src/services/requestor.service";
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { DatePipe } from '@angular/common';
import {
  FormGroup,
  FormBuilder,
  Validators,
  ValidatorFn,
  AbstractControl
} from "@angular/forms";
import { Router, ActivatedRoute } from "@angular/router";
import { Subject } from 'rxjs';
import { SweetAlertService } from 'src/services/sweet-alert.service';

@Component({
  selector: "app-contacts",
  templateUrl: "./contacts.component.html",
  styleUrls: ["./contacts.component.css"]
})
export class ContactsComponent implements OnDestroy, OnInit {
  form: FormGroup;
  invalidLogin = false;
  submitted = false;
  showModal: boolean;
  loading = false;
  display: 'none';
  
  get f() { return this.form.controls; }
  @ViewChild("closeAddContactModal", { static: false })
  closeAddContactModal: ElementRef;
  constructor(    
    private formBuilder: FormBuilder,
    private router: Router,
    public auth: AuthService,
    private http: HttpClient,
    private alert: SweetAlertService,
   // private apiService: RequestorService,
    private requestorservice: RequestorService,
    private datePipe: DatePipe,
   ) {  
  }


  show()
  {
    this.showModal = true; // Show-Hide Modal Check  
  }
  //Bootstrap Modal Close event
  hide()
  {
    this.showModal = false;
  }
  
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  public contactsList: any;
  public temp: Object=false;

  ngOnInit():void {

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength:10,
      processing:false
    };
    this.getRequestors();

    this.form = this.formBuilder.group({
      requestorName: ['', [Validators.compose([Validators.required,Validators.maxLength(20),Validators.minLength(4)])]],
      email: ['', Validators.compose([Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
      phoneNo: ['', Validators.compose([Validators.required, , Validators.maxLength(12),Validators.minLength(10),Validators.pattern("^[0-9]{10,12}$")])]

    })
  }

  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  getRequestors(): void {   

    this.requestorservice.getRequestors().subscribe(data => {     
      if (data.Code == 'SUC-200') {
        this.contactsList = JSON.parse(data.Data);
        this.dtTrigger.next();
        this.temp = true;
      }
      
    });

    // this.requestorservice.getRequestors()
    // .subscribe(
    //   data => {
    //     this.contacts = data;
    //     console.log(data);
    //   },
    //   error => {
    //     console.log(error);
    //   });
  }


  submitForm() {
    // console.log(this.form.value)
   // this.form.value

   this.submitted = true;

   if (this.form.invalid) {
    return;
  }
   
    if(this.submitted){
      this.showModal = false;
      //this.display='none';
     this.loading = true;
    
     this.requestorservice.addRequestor(this.form.value)
     .subscribe(data => {
      if (data.Code === 'SUC-200') {
       this.showModal = false;
      //  this.display='none';
      
      //  this.getRequestors();
        this.loading = false;
         this.onReset();
         this.alert.toastMesaage('success',"Added successfully");
     }
    // this.router.navigate(['contacts']);
     this.showModal = false;
     });
 
    }
    
   }

   onReset() {
    this.closeAddContactModal.nativeElement.click();  
    this.submitted = false;
    this.form.reset();    
  } 
  
}

import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import {
  NgbModalModule,
  NgbAlertModule,
  NgbPaginationModule,
  NgbModule
} from "@ng-bootstrap/ng-bootstrap";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { ReactiveFormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { BsDatepickerModule} from "ngx-bootstrap/datepicker";
import { TimepickerModule} from "ngx-bootstrap/timepicker";
import { PopoverModule} from "ngx-bootstrap/popover";

import { UserService } from "../services/userservice.services";
import { ContactApiService } from "../services/contacts.services";
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { RegisterComponent } from "./register/register.component";

import { Routes, RouterModule } from "@angular/router";
import { HeaderComponent } from "./layout/header/header.component";
import { LogoutComponent } from "./profile/logout/logout.component";
import { AccessdeniedComponent } from "./security/accessdenied/accessdenied.component";
import { HeaderwithoutmenuComponent } from "./layout/headerwithoutmenu/headerwithoutmenu.component";
import { ContactsComponent } from "./contacts/contacts.component";
import { AuthGuard } from "./guards/authguard/auth.guard";
import { RoleGuard } from "./guards/roleguard/role-guard.service";
import {CustomGuard} from "./guards/customguard/custom-guard.service"

import { FormsModule } from "@angular/forms";
import { TokenInterceptor } from "src/_helpers/interceptor";
import { ResetpasswordComponent } from "./resetpassword/resetpassword.component";
import { ForgotpasswordComponent } from "./forgotpassword/forgotpassword.component";
import { CompanyprofileComponent } from "./profile/companyprofile/companyprofile.component";
import { ProfileApiService } from "src/services/profileservice.services";
import "ag-grid-enterprise";

//import { FroalaEditorModule, FroalaViewModule } from "angular-froala-wysiwyg";
import { Dashboard360ViewComponent } from "./dashboard/dashboard360-view/dashboard360-view.component";
import { ChangepasswordComponent } from "./changepassword/changepassword.component";
import { dashboardAPIService } from "src/services/dashboard.services";
import { OrgChartModule } from "@mondal/org-chart";
import { ModalModule } from "ngx-bootstrap/modal";

import { DataTablesModule } from 'angular-datatables';

import { DecimalPipe, DatePipe} from "@angular/common";

import { NgSelectModule } from "@ng-select/ng-select";

import { AdminheaderComponent } from "./layout/adminheader/adminheader.component";

import { UtilitiesService } from "src/services/utilities.services";
import { UserprofileComponent } from "./profile/userprofile/userprofile.component";
import { NgxSpinnerModule } from "ngx-spinner";
import { TitleCaseDirective } from './title-case.directive';
import { UsersComponent } from './users/users.component';
import { IssuesComponent } from './issues/issues.component';
import { NotFoundComponent } from './custom_components/not-found/not-found.component';
import { ViewIssueComponent } from './issues/view-issue/view-issue.component';
import { AddIssueComponent } from './issues/add-issue/add-issue.component';
import { ViewContactComponent } from './contacts/view-contact/view-contact.component';
import { IssuesService } from 'src/services/issues.service';
import { FaqComponent } from './faq/faq.component';
import { FaqsComponent } from './faqs/faqs.component';
import { Faqs2Component } from './faqs2/faqs2.component';
import { FaqAddComponent } from './faq/faq-add/faq-add.component';
import { FaqViewComponent } from './faq/faq-view/faq-view.component';
import { LoaderComponent } from './loader/loader.component';
import { LoaderService } from 'src/services/loader.service';

const myRoots: Routes = [
  
  {
    path: "dashboard",
    component: Dashboard360ViewComponent,
    canActivate: [AuthGuard]
  },
  { path: "access-denied", component: AccessdeniedComponent },
  {
    path: "profile/company",
    component: CompanyprofileComponent,
    canActivate: [AuthGuard]
  },
  
  { path: "logout", component: LogoutComponent },
  { path: "notfound", component: NotFoundComponent },  
  {
    path: "user/profile",
    component: UserprofileComponent,
    canActivate: [AuthGuard]
  },  
  { path: "resetpassword", component: ResetpasswordComponent },
  { path: "forgotpassword", component: ForgotpasswordComponent },
  
  { path: "contactslist", component: ContactsComponent, canActivate: [AuthGuard] },
  { path: "contacts", component: ContactsComponent ,canActivate: [AuthGuard]},
  { path: "view-contact", component: ViewContactComponent ,canActivate: [AuthGuard]},

  { path: "issues", component: IssuesComponent, canActivate: [AuthGuard] },
  { path: "view-issue", component: ViewIssueComponent, canActivate: [AuthGuard] },
  { path: "add-issue", component: AddIssueComponent, canActivate: [AuthGuard] },

  { path: "faq", component: FaqComponent, canActivate: [AuthGuard] },
  { path: "faq-add/:id/:topicintid", component: FaqAddComponent, canActivate: [AuthGuard] },
  { path: "faq/:id", component: FaqViewComponent, canActivate: [AuthGuard] },
  
  // { path: '', component: LoginComponent,pathMatch: "full" },
  {
    path: "admin/users",
    component: UsersComponent,
    canActivate: [AuthGuard]
  },
  { path: "changepassword", component: ChangepasswordComponent },
  { path: "", redirectTo: "/dashboard", pathMatch: "full" },
  {
    path: "**",
    component: NotFoundComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,  
    HeaderComponent,
    LogoutComponent,
    AccessdeniedComponent,
    HeaderwithoutmenuComponent,
    ContactsComponent,
    LoaderComponent,
    NotFoundComponent,
    AdminheaderComponent,
    ResetpasswordComponent,
    ForgotpasswordComponent,
    CompanyprofileComponent, 
    Dashboard360ViewComponent,
    ChangepasswordComponent, 
    UserprofileComponent,
    TitleCaseDirective,
     UsersComponent, IssuesComponent, ViewIssueComponent, AddIssueComponent, ViewContactComponent, FaqComponent, FaqsComponent, Faqs2Component, FaqAddComponent, FaqViewComponent
  ],
  imports: [
    NgSelectModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
    NgbModalModule,
    FormsModule,
    BrowserAnimationsModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    PopoverModule.forRoot(),
  //  FroalaEditorModule.forRoot(),
   // FroalaViewModule.forRoot(),
    OrgChartModule,
    NgxSpinnerModule,
    NgbPaginationModule,
    NgbAlertModule,
    DataTablesModule,
    ModalModule.forRoot(),
    RouterModule.forRoot(
      myRoots,
      { enableTracing: true } // <-- debugging purposes only
    ),
    ModalModule.forRoot()
  ],
  providers: [
    UserService,
    ContactApiService,
    ProfileApiService,    
    IssuesService,
    DecimalPipe,
    DatePipe,
    UtilitiesService,
    DecimalPipe,
    LoaderService,
    dashboardAPIService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  entryComponents: [     
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

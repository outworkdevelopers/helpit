import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from "src/app/guards/authguard/auth.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  constructor(private router: Router,public auth:AuthService) {
    this.router.errorHandler = (error: any) => {
        let routerError = error.toString();
        if (routerError.indexOf('Cannot match any routes') >= 0 ) {
           this.router.navigate(['/notfound']);
        } else {
           throw error;
        }
    }
  }
}

import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {first} from "rxjs/operators";
import { ApiResponse } from 'src/model/apiresponse.model';
import { IssuesService } from "src/services/issues.service";
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { HttpClient } from '@angular/common/http';
import { SweetAlertService } from 'src/services/sweet-alert.service';

@Component({
  selector: 'app-view-issue',
  templateUrl: './view-issue.component.html',
  styleUrls: ['./view-issue.component.css']
})
export class ViewIssueComponent implements OnInit {

  editForm: FormGroup;
  submitted = false;
  updatedata:any;
  issuedata: any;
  constructor(private formBuilder: FormBuilder,private router: Router, private issuesservice: IssuesService, public auth: AuthService, private alert: SweetAlertService) { }

  getControlLabel(type: string){
    return this.editForm.controls[type].value;
   }


   public statusList: any;
   public categoryList: any;
   public channelsList: any;
   public IssueIntId:any;
  ngOnInit() {


    this.getCategories();
    this.getChannels();
    this.getStatus();

     let issueId = window.localStorage.getItem("editIssueId");
    // this.updatedata={};
    console.log('issueid - ', issueId );

    if(!issueId) {
      alert("Invalid action.")
      this.router.navigate(['issues']);
      return;
    }
    this.editForm = this.formBuilder.group({
      AssignedToId:[''],
      AssignedToIntId: [''],
      CategoryId: [''],
       CategoryIntId: [''],
      CreatedBy: [''],
      CreatedDate: [''],
      Description: [''],
     Id: [''],
     IsDeleted: [''],
      IssueId: [''],
       IssueStatus: [''],
       IssueUserType: [''],
      ModifiedBy: [''],
      ModifiedDate: [''],
      ObjectId: [''],
      ObjectIntId: [''],
      ObjectType: [''],
      OrganizationId: [''],
      OrganizationIntId: [''],
      OwnerId: [''],
      OwnerIntId: [''],
      ParentId: [''],
      ParentIntId: [''],
      Priority: [''],
     RequestorId: [''],
     RequestorIntId: [''],
      RequestorName: [''],
      ResolutionDate: [''],
      ResolutionNotes: [''],
      Source: [''],
      Status: [''],
      SubCategoryId: [''],
      SubCategoryIntId: [''],
      Tags: [''],
     Title: [''],
     CreatedByName:['Shravan'],
     UserType: ['']
    });

    this.issuesservice.getIssueById(+issueId)
      .subscribe( data => {     
        let issueInfoObject = JSON.parse(data.Data);
       // issueInfoObject.Status = 1;
       console.log('ggg',issueInfoObject);
       this.editForm.setValue(issueInfoObject);
      
      // this.editForm.setValue({Title: issueInfoObject.Title, RequestorName: issueInfoObject.RequestorName, CategoryIntId: issueInfoObject.CategoryIntId, Description: issueInfoObject.Description,IssueId: issueInfoObject.IssueId});
      
      });  
  }

  
  getCategories(): void {
    this.issuesservice.getCategories().subscribe(data => {     
      if (data.Code == 'SUC-200') {
        this.categoryList = JSON.parse(data.Data);
      } 
    });
  }

  getChannels(): void {
    this.issuesservice.getChannels().subscribe(data => {     
      if (data.Code == 'SUC-200') {
        this.channelsList = JSON.parse(data.Data);
      } 
    });
  }

  getStatus(): void {
    this.issuesservice.getStatus().subscribe(data => {     
      if (data.Code == 'SUC-200') {
        this.statusList = JSON.parse(data.Data);
      } 
    });
  }
  
  onSubmit() {

    this.submitted = true;
  //  this.IssueIntId=this.editForm.value.Id;
  //  console.log('issueintid',this.IssueIntId);
    this.issuesservice.updateIssue(this.editForm.value).pipe(first())
      .subscribe(
        data => {
          if(data.Code ===  'SUC-200') {
            this.alert.toastMesaage('success',"Added successfully");
            this.router.navigate(['/issues']);
          }
          else {
            alert(data.Message);
            this.alert.toastMesaage('error',data.Message);
            console.log('udata- ',this.editForm.value);
          }
        }
        ,error => {
          alert(error);
        }
        );
        
  }

  

}
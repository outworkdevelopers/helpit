import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup , Validators} from "@angular/forms";
import { HttpClient } from '@angular/common/http';
import { IssuesService } from 'src/services/issues.service';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { RequestorService } from "src/services/requestor.service";

import { MustMatch } from 'src/_helpers/must-match.validator';
import { SweetAlertService } from 'src/services/sweet-alert.service';

@Component({
  selector: 'app-add-issue',
  templateUrl: './add-issue.component.html',
  styleUrls: ['./add-issue.component.css']
})

export class AddIssueComponent implements OnInit {
  issueform: FormGroup;
  submitted = false;
  constructor( 
    public formBuilder: FormBuilder,
    private http: HttpClient,
    public auth: AuthService,
    private router:Router,
    private alert: SweetAlertService,
    private requestorservice: RequestorService,
    public issueservice:IssuesService) 
    {}

  
  public contactsList: any;
  public categoryList: any;
  public channelsList: any;


  ngOnInit() {
    
    this.getRequestors();
    this.getCategories();
    this.getChannels();

    this.issueform = this.formBuilder.group({
      CategoryIntId: ['', Validators.required],
      Description: ['', Validators.required],
      Priority: ['', Validators.required],
      RequestorIntId: ['', Validators.required],
      Source: ['', Validators.required],
      Status: ['1'],
      Title:  ['', Validators.required],
    })
  }

   // convenience getter for easy access to form fields
   get f() { return this.issueform.controls; }

  getRequestors(): void {
    this.requestorservice.getRequestors().subscribe(data => {     
      if (data.Code == 'SUC-200') {
        this.contactsList = JSON.parse(data.Data);
      } 
    });
  }

  getCategories(): void {
    this.issueservice.getCategories().subscribe(data => {     
      if (data.Code == 'SUC-200') {
        this.categoryList = JSON.parse(data.Data);
      } 
    });
  }

  getChannels(): void {
    this.issueservice.getChannels().subscribe(data => {     
      if (data.Code == 'SUC-200') {
        this.channelsList = JSON.parse(data.Data);
      } 
    });
  }

  

  submitForm() {
    this.submitted = true;
    if(this.issueform.valid){
     
      this.issueservice.addissue(this.issueform.value).subscribe(data => {
        // console.log('issues',res);
        if (data.Code === 'SUC-200') {
          this.alert.toastMesaage('success',"Issue added successfully");
          this.router.navigate(['/issues']);
        }
      })
    }

    // stop here if form is invalid
    if (this.issueform.invalid) {
      return;
  }
   
  }


}

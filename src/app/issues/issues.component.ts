import { Component, OnInit, ElementRef, ViewChild, OnDestroy, TemplateRef } from "@angular/core";
import { IssuesService } from "src/services/issues.service";
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { Router, ActivatedRoute } from "@angular/router";
import { element } from 'protractor';

//import { DatePipe } from '@angular/common';
//import {FormGroup,FormBuilder, Validators, ValidatorFn, AbstractControl } from "@angular/forms";
// import { ContactApiService } from 'src/services/contacts.services';

@Component({
  selector: 'app-issues',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.css']
})
export class IssuesComponent implements OnInit{
  dtOptions: DataTables.Settings = {};
  public issuesList: any;
  public displayList: any;
  public temp: Object=false;
  constructor(
  //  private formBuilder: FormBuilder, private datePipe: DatePipe, private contactService:ContactApiService,
    private router: Router,    
    public auth: AuthService,
    private issuesservice: IssuesService
  ) { }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength:10,
      processing:true
    };
    this.getIssues();
  }

  getIssues() {
    this.issuesservice.getIssues().subscribe(data => {
      if (data.Code === 'SUC-200') {
        this.issuesList = JSON.parse(data.Data);

        this.temp = true; 
        console.log(this.issuesList);
      } 
    }); 
  }

  editIssue(issue: any): void {
  //  console.log('issueid - ',issue.Id);
   window.localStorage.removeItem("editIssueId");
    // window.localStorage.setItem("editIssueId", issue.toString());
    window.localStorage.setItem("editIssueId", issue.Id.toString());
    this.router.navigate(['view-issue']);
  };
  onChange(filter) {
    console.log(filter);
    switch (filter) {
      case 'All':
        // this.displayList = this.issuesList
        break;
      case 'Today':
        // let today = new Date();
        // this.displayList = this.issuesList.filter(element => {
        //   console.log(element.CreatedDate);
        //   let day = new Date(element.CreatedDate);
        //   console.log(day);
        //   if(day.getDay == today.getDay && day.getMonth == today.getMonth && day.getFullYear == today.getFullYear){
            
        //     return element;
        //   }
          
        // })
        // console.log(this.displayList.count);
        
        break;
       
    
      default:
      case 'This Week':
       
        
        
        // fromDate = toDateStr.split(", ")[0];
        

//         var curr = new Date; // get current date
// var first = curr.getDate() - curr.getDay(); // First day is the day of the month - the day of the week
// var last = first + 6; // last day is the first day + 6

// var firstday = new Date(curr.setDate(first)).toUTCString();
// var lastday = new Date(curr.setDate(last)).toUTCString();
// console.log(firstday);
// console.log(lastday);

        break;
    }
}

}

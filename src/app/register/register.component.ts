import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from '../../services/userservice.services';
import { UtilitiesService } from "src/services/utilities.services";
import { FormGroupDirective } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  invalidLogin = false;
  submitted = false;
  loading = false;
  get f() { return this.registerForm.controls; }
  constructor(private formBuilder: FormBuilder, private router: Router, private userService: UserService,
    public utilities: UtilitiesService) { }
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      
      loginId: ['', Validators.compose([Validators.required, Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")])],
      firstName: ['',[Validators.required,Validators.maxLength(20),Validators.minLength(4)]],
     // loginId:  ['', [Validators.required, Validators.email,Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
      password: ['',[Validators.required,Validators.maxLength(20),Validators.minLength(4)]],
      organizationName:  ['',[Validators.required,Validators.maxLength(20),Validators.minLength(4)]],
     // lastName: [],
      // Email: ['', [Validators.required, Validators.email,
      //   Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]]
    });
  }

  showMsg: boolean = false;
  
  onSubmit() {
  
    this.submitted = true;
    this.loading = true;
    console.log('naidu');
    if (this.registerForm.invalid) {
      console.log('user not created');
      this.loading = false;
      return;
    }
    this.userService.register(this.registerForm.value).subscribe(data => {
     // console.log('successfully user created',this.registerForm.value);
      if (data.Code === 'SUC-200') {
    //    console.log('successfully user created.....');
        // this.router.navigate(['/register']);
        this.showMsg= true;
        this.loading = false;
      //  this.registerForm=null;
     
       this.registerForm.reset();  
        
          // this.registerForm.reset({
          //       'firstName': '',
          //       'password': '',
          //        'organizationName': ''
          //      });
       
      }
      else {

      }
    });
  }


}

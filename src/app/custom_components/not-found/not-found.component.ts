import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/guards/authguard/auth.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.css']
})
export class NotFoundComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit() {
  }

}

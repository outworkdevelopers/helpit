import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { AuthService } from '../authguard/auth.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CustomGuard implements CanActivate{ 
  constructor(private auth: AuthService, private _router: Router) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      
      let configCheck = this.auth.getEmailConfig();
      let isFirstRedirection = this.auth.getIsFirstTimeRedirection();
      if(configCheck === "false" && isFirstRedirection == "yes"){
        this.auth.setIsFirstTimeRedirection("no");
        this._router.navigate(["user/profile"]);
        return false;
      }else{        
        return true;
      }    
  }
}



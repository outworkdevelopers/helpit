import { JsonPipe } from '@angular/common';
import { ThrowStmt } from '@angular/compiler';
import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { FaqCategoryService } from 'src/services/faq.category.service';
import { FaqService } from 'src/services/faq.service';
import { AuthService } from '../guards/authguard/auth.service';
declare var jQuery: any;
declare var $;

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {

  @ViewChild('catClose', { static: false }) catCloseBtn;
  @ViewChild('catEditClose', { static: false }) catEditClose;
  @ViewChild('topicClose', { static: false }) topicCloseBtn;
  @ViewChild('topicEditClose', { static: false }) topicEditClose;
  
  faqs: any=[];
  faqCategoryForm:FormGroup;
  updateCategoryForm:FormGroup;
  faqCategories:any;
  CatIndex :any;
  TopicId:any;
  selectedCategories:number[] = [];
  Cid:any;
  Tid:any;
  Cname:any;
  Tname:any='';
  
  constructor(private router: Router,   private formBuilder: FormBuilder,  
    public auth: AuthService, private route: ActivatedRoute,
    private faqService: FaqService,
private faqCategoryService: FaqCategoryService) {

    this.faqCategoryForm = this.formBuilder.group({
      CategoryN:['', [Validators.required,this.noWhitespaceValidator]]
    });

    this.updateCategoryForm = this.formBuilder.group({
      CategoryN:['',[Validators.required]]
    });

    this.route.params.subscribe(params => {    // taking id from params

      this.Cid = params['id']; 
      this.Tid=params['topicintid'];
      console.log(this.Cid);
      console.log(this.Tid);
    });

  }

  noWhitespaceValidator(control: FormControl) {
    const isWhitespace = (control && control.value && control.value.toString() || '').trim().length === 0;
    const isValid = !isWhitespace;
    return isValid ? null : { 'whitespace': true };
  }

  ngOnInit() {
    (function ($) {
      $(document).ready(function(){
        $('ul.tree ul').hide();

        $('.tree li > ul').each(function(i) {
          var $subUl = $(this);
          var $parentLi = $subUl.parent('li');
          var $toggleIcon = '<img src="assets/img/down-arrow.svg" class="js-toggle-icon" style="height:15px;">';
      
          $parentLi.addClass('has-children');
          
          $parentLi.prepend( $toggleIcon ).find('.js-toggle-icon').on('click', function() {
           // $(this).text( $(this).text() == '+' ? '-' : '+' );
            $subUl.slideToggle('fast');
          });
        });
    });
    })(jQuery);

    this.GetAllFaqCategories();
   // this.getAllFaqs();

  }

  resetForm(){
    console.log('reset form********');
    this.faqCategoryForm.reset();
  }

  CreateFaqCategory()
  {

    if(this.faqCategoryForm.valid){
      console.log("create category");
      var data={
      
          "id": 0,
          "categoryId": null,
          "organizationIntId": 0,
          "organizationId": null,
          "botIntId": 0,
          "botId": null,
          "parentIntId": 0,
          "parentId": null,
          "categoryName":this.faqCategoryForm.value['CategoryN'],
          "description": null,
          "createdDate": null,
          "modifiedDate": null,
          "createdBy": null,
          "modifiedBy": null,
          "isDeleted": false
        }
  
      console.log(data);
  
      this.faqCategoryService.CreateFaqCategory(data).subscribe(respData => {
        this.faqCategoryForm.reset();
        
        if (respData.Code === 'SUC-200') {
          // this.router.navigate(['/faq']);
          this.catCloseBtn.nativeElement.click();
          this.GetAllFaqCategories();
        }
      });
    }else{

    }
    
  }


  GetAllFaqCategories(){
    this.faqCategoryService.getAllFaqCategories().subscribe(data => {
      if (data.Code === 'SUC-200') {
        this.faqCategories = JSON.parse(data.Data);
        console.log(this.faqCategories);
        this.Cname=this.faqCategories[0].CategoryName;
        this.Tname=this.faqCategories[0].SubCategories[0].CategoryName;
        this.Cid=this.faqCategories[0].Id;
        this.Tid=this.faqCategories[0].SubCategories[0].CategoryIntId;
        this.selectedCategories.push(0);
        this.getAllFaqs();
      }
    });
  }

  getAllFaqs() {
    console.log('get all faqs');
    console.log(this.Cid);
    console.log(this.Tid);
    this.faqService.getAllFaqs(this.Cid,this.Tid).subscribe(data => {
      if (data.Code === 'SUC-200') {
        if(data.Data!=undefined){
          this.faqs = JSON.parse(data.Data);
        }

        console.log(this.faqs);
      } 
      else
      console.log(data.Data);
    }); 
  }

  categoryClicked(selectedIndex){
    this.Tid="";
    this.Tname="";
    this.Cid=this.faqCategories[selectedIndex].Id;
    this.Cname=this.faqCategories[selectedIndex].CategoryName;
    console.log('cat id for add '+this.Cid);
    if(this.selectedCategories.indexOf(selectedIndex) >= 0){
      //this.selectedCategories.splice(this.selectedCategories.indexOf(selectedIndex), 1)
      this.selectedCategories=[];
    }else{
      this.selectedCategories=[];
      this.selectedCategories.push(selectedIndex);
      this.CatIndex=selectedIndex;
    }
  }

  SetFaqCategoryValues(selectedCatId){
    console.log('clicked');
    console.log(selectedCatId);
    this.updateCategoryForm.controls['CategoryN'].setValue(this.faqCategories[selectedCatId].CategoryName);
    this.CatIndex=selectedCatId;
  }

  UpdateFaqCategory(){

   let  updateData={
    "categoryName":this.updateCategoryForm.value['CategoryN'],
    }

    console.log('update entered');
    var catId=this.faqCategories[this.CatIndex].Id;
    this.faqCategoryService.updateFaqCategoryById(updateData,catId).
    subscribe(data => {
      if (data.Code === 'SUC-200') {
        this.catEditClose.nativeElement.click();
        this.GetAllFaqCategories();
      } 
    }); 
  }

  CreateTopic()
  {
    console.log("create topic");
    console.log(this.CatIndex);

    
    var data={
        "parentIntId": this.faqCategories[this.CatIndex].ParentIntId,
        "parentId": this.faqCategories[this.CatIndex].ParentId,
        "categoryName":this.faqCategoryForm.value['CategoryN'],
      }
    console.log(data);

    this.faqCategoryService.Createtopic(data).subscribe(respData => {
      this.faqCategoryForm.reset();
      if (respData.Code === 'SUC-200') {
        this.topicCloseBtn.nativeElement.click();
        this.GetAllFaqCategories();
      }
    });
  }

  SetTopicValues(subcategory){
    this.Tid=subcategory.CategoryIntId;
    this.Tname=subcategory.CategoryName;
    this.getAllFaqs();
    console.log('topicid for add '+this.Tid);
    this.updateCategoryForm.controls['CategoryN'].setValue(subcategory.CategoryName);
    this.TopicId=subcategory.CategoryIntId;
  }

  UpdateTopic(){

    let  updateData={
     "categoryName":this.updateCategoryForm.value['CategoryN'],
     }
 
     console.log('update entered');
     console.log(this.TopicId);
     this.faqCategoryService.updateFaqCategoryById(updateData,this.TopicId).
     subscribe(data => {
       if (data.Code === 'SUC-200') {
         this.topicEditClose.nativeElement.click();
         this.GetAllFaqCategories();
       } 
     }); 
   }
}

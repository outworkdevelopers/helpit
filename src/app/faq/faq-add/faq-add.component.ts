import { Component, OnInit } from '@angular/core';
import { FormArray } from '@angular/forms';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/guards/authguard/auth.service';
import { FaqService } from 'src/services/faq.service';
declare var jQuery: any;
@Component({
  selector: 'app-faq-add',
  templateUrl: './faq-add.component.html',
  styleUrls: ['./faq-add.component.css']
})
export class FaqAddComponent implements OnInit {

  faqForm: FormGroup;
  Catid:any;
  Topicid:any;

  constructor(private formBuilder: FormBuilder, private router: Router,
    public auth: AuthService, private route: ActivatedRoute,
    private faqService: FaqService) {
    this.faqForm = this.formBuilder.group({
      primaryQ: ['', [Validators.required]],
      answer: ['', [Validators.required]],
      variants: new FormArray([]),
    });

    this.route.params.subscribe(params => {    // taking id from params

      this.Catid = params['id']; 
      this.Topicid=params['topicintid'];
      console.log(this.Catid);
      console.log(this.Topicid);
    });


  }

  ngOnInit() {

  }

  addClicked() {
    this.t.push(this.formBuilder.group({
      question: ['', Validators.required],
    }));
  }

  get f() { return this.faqForm.controls; }
  get t() { return this.f.variants as FormArray; }

  submitQuestion() {

    if(this.faqForm.valid){
      var variants = []
      if (this.faqForm.value['variants'].length > 0) {
        this.faqForm.value['variants'].forEach(element => {
          variants.push(element['question'])
        });
      }
      var data = {
        "botIntId": 0,
        "botId": "",
        "categoryIntId":this.Catid,
        "categoryId": "",
        "topicIntId":this.Topicid,
        "topicId": "",
        "primaryQuestion": this.faqForm.value['primaryQ'],
        "variantQuestions": variants,
        "response": this.faqForm.value['answer'],
        "createdDate": "",
        "modifiedDate": "",
        "createdBy": "",
        "modifiedBy": ""
      }
  
      console.log('question submitted');
      console.log(data);
  
      this.faqService.submitFaq(data).subscribe(respData => {
        if (respData.Code === 'SUC-200') {
          this.router.navigate(['/faq']);
          console.log(respData);
        }
      });
    }else{
      console.log('invalid data');
    }
   
    
  }
}
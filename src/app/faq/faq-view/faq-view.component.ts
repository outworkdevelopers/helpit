import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { group } from 'console';
import {first} from "rxjs/operators";
import { FaqService } from 'src/services/faq.service';
declare var jQuery: any;
@Component({
  selector: 'app-faq-view',
  templateUrl: './faq-view.component.html',
  styleUrls: ['./faq-view.component.css']
})
export class FaqViewComponent implements OnInit {
  updateFaqForm: FormGroup;
  singleFaq:any;
  selectedId : any;


  constructor(private formBuilder: FormBuilder,
    private route: ActivatedRoute,private router: Router,
    private faqService: FaqService) {
  
    this.route.params.subscribe(params => {    // taking id from params

      this.selectedId = params['id']; 
      console.log(this.selectedId);
   });
  }

 
  

  ngOnInit() {
    (function ($) {
      $(document).ready(function(){
        $("#Add").on("click", function() {  
          $("#textboxDiv").append("<div class='form-group'><input type='text' class='form-control'></div>");  
      });  
    });
    })(jQuery);

    this.updateFaqForm = this.formBuilder.group({
      PrimaryQuestion: [''],
      Response: [''],
      Variants:new FormArray([]),
      variantQ1:[''],
      variantQ2:[''],
      variantQ3:[''],
      variantQ4:[''],
      variantQ5:[''],

    });

    this.getFaqById();
  }

  getFaqById() {
    this.faqService.getFaqById(this.selectedId).subscribe(data => {
      if (data.Code === 'SUC-200') {
        this.singleFaq = JSON.parse(data.Data);
        console.log(this.singleFaq); 
        this.FromFaq();
      } 
    }); 
  }

  addClicked(question) {
    this.t.push(this.formBuilder.group({
      question: [question, Validators.required],
    }));
  }

  get f() { return this.updateFaqForm.controls; }
  get t() { return this.f.Variants as FormArray; }

  FromFaq()
  {
    console.log('fromfaq');
    this.updateFaqForm.controls['PrimaryQuestion'].setValue(this.singleFaq.PrimaryQuestion);
    this.updateFaqForm.controls['Response'].setValue(this.singleFaq.Response);
    console.log(this.singleFaq.VariantQuestions.length);
    // this.singleFaq.VariantQuestions.forEach( (currentValue, index) => {
    //   switch(index){
    //     case 0:{
    //       this.updateFaqForm.controls['variantQ1'].setValue(currentValue);
    //       break;
    //     }
    //     case 1:{
    //       this.updateFaqForm.controls['variantQ2'].setValue(currentValue);
    //       break;
    //     }
    //     case 2:{
    //       this.updateFaqForm.controls['variantQ3'].setValue(currentValue);
    //       break;
    //     }
    //     case 3:{
    //       this.updateFaqForm.controls['variantQ4'].setValue(currentValue);
    //       break;
    //     }
    //     case 4:{
    //       this.updateFaqForm.controls['variantQ5'].setValue(currentValue);
    //       break;
    //     }
    //   }

    //   })
    var questions=[]
    console.log(this.singleFaq.VariantQuestions);
    this.singleFaq.VariantQuestions.forEach( (element) => {
      this.addClicked(element);
      console.log(element);
         //this.updateFaqForm.controls['question'].setValue(element['question']);

    });
   // this.updateFaqForm.controls['Variants'].setValue(questions);

  }
  
ToFaq(){
  var variants = []
  if (this.updateFaqForm.value['Variants'].length > 0) {
    this.updateFaqForm.value['Variants'].forEach(element => {
      variants.push(element['question'])
    });
  }
  // if(this.updateFaqForm.value['variantQ1'].length > 0){
  //   variants.push(this.updateFaqForm.value['variantQ1'])
  // }
  // if(this.updateFaqForm.value['variantQ2'].length > 0){
  //   variants.push(this.updateFaqForm.value['variantQ2'])
  // }
  // if(this.updateFaqForm.value['variantQ3'].length > 0){
  //   variants.push(this.updateFaqForm.value['variantQ3'])
  // }
  // if(this.updateFaqForm.value['variantQ4'].length > 0){
  //   variants.push(this.updateFaqForm.value['variantQ4'])
  // }
  // if(this.updateFaqForm.value['variantQ5'].length > 0){
  //   variants.push(this.updateFaqForm.value['variantQ5'])
  // }
this.singleFaq.VariantQuestions=variants;
this.singleFaq.PrimaryQuestion=this.updateFaqForm.value['PrimaryQuestion'];
this.singleFaq.Response=this.updateFaqForm.value['Response'];
}

onSubmit(){

this.ToFaq();
  
  console.log('update entered')
  this.faqService.updateFaqById(this.singleFaq,this.selectedId).
  subscribe(data => {
    if (data.Code === 'SUC-200') {
      this.singleFaq = JSON.parse(data.Data);
      this.router.navigate(['/faq']);
      console.log(this.singleFaq); 
    } 
  }); 
}
}

